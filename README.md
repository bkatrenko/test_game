# Test game backend for golang

App have a seven routes (Please note, that some methods have request body): 
> - POST /announceTournament (Example: {"deposit":100, "players":9, "duration_sec":61})
> - PATCH /take (Example: {"player_name":"P1", "points":1000})
> - PATCH /fund (Example: {"player_name":"P1", "points":1000})
> - POST /joinTournament (Example: {"player_name":"P2", "tournament_id":10, "backers":["P1", "P3"]}) 
> - GET	/resultTournament/{id}
> - GET	/balance/{name}
> - PATCH /reset

Images on docker hub: 

> - bkatrenko/testgame_db
> - bkatrenko/testgame_app

You can run app in simple way:
> - git clone https://bitbucket.org/bkatrenko/test_game
> - cd $GOPATH/bitbucket.org/bkatrenko/test_game
> - docker-compose up --build

Or use docker hub images. 
For run tests: 
> - cd $GOPATH/bitbucket.org/bkatrenko/test_game
> - docker-compose up --build
> - go test ./... (should be called from another terminal window)

Tests contain test for game logic from technical requirements. So, it run "/fund" for P1, P2, P3, P4, P5 players, announce new tournament, get winner and check existing player's balance. This test has name "TestGameLogic".