CREATE TABLE players (
      id SERIAL PRIMARY KEY,
      points INT NOT NULL DEFAULT 0 CHECK (points > -1),
      name VARCHAR(50) NOT NULL DEFAULT '' UNIQUE
);
          
CREATE TABLE tournaments (
      id SERIAL PRIMARY KEY,
      deposit INT NOT NULL DEFAULT 0 CHECK (deposit > 0),
      max_players INT NOT NULL DEFAULT 2 CHECK (max_players > 1),
      players INT[] DEFAULT array[]::int[],
      winners INT[] DEFAULT array[]::int[],
      start_time timestamp NOT NULL,
      end_time timestamp NOT NULL
);

CREATE TABLE backers (
      id SERIAL PRIMARY KEY,
      player_name VARCHAR(50) REFERENCES players(name),
      player_id INT REFERENCES players(id), 
      tournament_id INT REFERENCES tournaments(id),
      backers INT[] DEFAULT array[]::int[],
      deposit INT NOT NULL DEFAULT 0 CHECK (deposit > 0)    
);