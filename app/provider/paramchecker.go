package provider

import (
	"bitbucket.org/bkatrenko/test_game/app/model"
	"bitbucket.org/bkatrenko/test_game/app/tools/jsonconfig"
)

// CheckAnnounceTournamentParams get new tournament input and return bad request error or nil if all right
func CheckAnnounceTournamentParams(tournament *model.TournamentJSONInput) *model.BadRequestError {
	if tournament.Deposit <= 0 {
		return model.NewBadRequestError("deposit", "deposit less or equal to zero")
	}

	if tournament.Deposit < jsonconfig.Tournament().MinDeposit {
		return model.NewBadRequestError("deposit", "deposit less than the minimum allowed")
	}

	if tournament.DurationSec < jsonconfig.Tournament().MinDurationSec {
		return model.NewBadRequestError("duration", "duration less than the minimum allowed")
	}

	if tournament.Players < 2 {
		return model.NewBadRequestError("players", "players count < 2")
	}

	if tournament.Players > jsonconfig.Tournament().MaxPlayers {
		return model.NewBadRequestError("players", "players count bigger than the maximum allowed")
	}

	return nil
}

// CheckChangePointsParams check take points input for bad params - less that zero for example
func CheckChangePointsParams(input *model.ChangePointsJSONInput) error {
	if input.PlayerName == "" {
		return model.NewBadRequestError("player_id", "player name is 0")
	}

	if input.Points <= 0 {
		return model.NewBadRequestError("points", "points is less or equal to 0")
	}

	return nil
}

// CheckJoinTournamentParams chack player name and not 0 join tournament params
func CheckJoinTournamentParams(joinParams *model.JoinTournamentJSONInput) error {
	if joinParams.PlayerName == "" {
		return model.NewBadRequestError("player_name", "player_name is empty")
	}

	if joinParams.TournamentID == 0 {
		return model.NewBadRequestError("tournament_id", "tournament_id is 0")
	}

	if joinParams.TournamentID < 0 {
		return model.NewBadRequestError("tournament_id", "tournament_id less than 0")
	}

	return nil
}
