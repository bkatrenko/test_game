package provider

import (
	"bitbucket.org/bkatrenko/test_game/app/model"
)

func getBackersIDs(backers []*model.Player) []int64 {
	IDs := []int64{}

	for _, v := range backers {
		IDs = append(IDs, v.ID)
	}

	return IDs
}

// util method for int64 slices
func contains(s []int64, e int64) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
