package provider

import (
	"log"
	"os"
	"testing"

	"bitbucket.org/bkatrenko/test_game/app/model"
	"bitbucket.org/bkatrenko/test_game/app/tools/jsonconfig"
)

func TestParamChecker(t *testing.T) {

	err := jsonconfig.Load("../../config"+string(os.PathSeparator)+"config.json", jsonconfig.AppConfig)
	if err != nil {
		log.Fatal("error while parse config file: ", err)
	}

	t.Run("TestAnnounceParamChecker", func(t *testing.T) {
		var err error

		err = CheckAnnounceTournamentParams(&model.TournamentJSONInput{
			Deposit:     jsonconfig.Tournament().MinDeposit - 1,
			Players:     jsonconfig.Tournament().MaxPlayers,
			DurationSec: jsonconfig.Tournament().MinDurationSec,
		})
		if err == nil {
			t.Error("error while test param checker: error expected, deposit is wrong")
		}

		err = CheckAnnounceTournamentParams(&model.TournamentJSONInput{
			Deposit:     jsonconfig.Tournament().MinDeposit,
			Players:     0,
			DurationSec: jsonconfig.Tournament().MinDurationSec,
		})
		if err == nil {
			t.Error("error while test param checker: error expected, max players is 0")
		}

		err = CheckAnnounceTournamentParams(&model.TournamentJSONInput{
			Deposit:     jsonconfig.Tournament().MinDeposit,
			Players:     jsonconfig.Tournament().MaxPlayers + 1,
			DurationSec: jsonconfig.Tournament().MinDurationSec,
		})
		if err == nil {
			t.Error("error while test param checker: error expected, max players is wrong")
		}

		err = CheckAnnounceTournamentParams(&model.TournamentJSONInput{
			Deposit:     jsonconfig.Tournament().MinDeposit,
			Players:     jsonconfig.Tournament().MaxPlayers,
			DurationSec: jsonconfig.Tournament().MinDurationSec - 1,
		})
		if err == nil {
			t.Error("error while test param checker: error expected, duration is wrong")
		}

	})

	t.Run("TestChangePointsChecker", func(t *testing.T) {
		var err error

		err = CheckChangePointsParams(&model.ChangePointsJSONInput{
			PlayerName: "",
			Points:     1,
		})
		if err == nil {
			t.Error("error while test param checker: error expected, player ID is 0")
		}

		err = CheckChangePointsParams(&model.ChangePointsJSONInput{
			PlayerName: "P",
			Points:     0,
		})
		if err == nil {
			t.Error("error while test param checker: error expected, points is 0")
		}
	})

}
