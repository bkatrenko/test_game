package provider

import (
	"database/sql"
	"errors"
	"log"
	"time"

	"bitbucket.org/bkatrenko/test_game/app/model"
	"bitbucket.org/bkatrenko/test_game/app/tools/dbutils"
	"bitbucket.org/bkatrenko/test_game/app/tools/jsonconfig"
)

var (
	database *sql.DB
)

// GetDBConnection return current DB connection (using for testing only)
func GetDBConnection() *sql.DB {
	return database
}

// CloseDBConnection close current connection and return error if smth went wrong
func CloseDBConnection() error {
	return database.Close()
}

// ConnectToDB do init provider's database connection
func ConnectToDB(params *jsonconfig.DBConfig) error {
	var err error

	database, err = dbutils.CreateNewDBConnection(params)
	if err != nil {
		log.Println("error while create new DB connection: " + err.Error())
		return err
	}

	return nil
}

// AnnounceTournament do params checking and work with DB for announce new tournament
func AnnounceTournament(tournament *model.TournamentJSONInput) (int64, error) {
	if tournament == nil {
		return 0, errors.New("error while announce tournament: request params is nil")
	}

	badRequestErr := CheckAnnounceTournamentParams(tournament)
	if badRequestErr != nil {
		log.Println("warning: can't announce new tournament: bad request params: ", badRequestErr)
		return 0, badRequestErr
	}

	dbTournament := &model.Tournament{
		Deposit:    tournament.Deposit,
		MaxPlayers: tournament.Players,
		StartTime:  time.Now(),
		EndTime:    time.Now().Local().Add(time.Second * time.Duration(tournament.DurationSec)),
	}

	tx, err := database.Begin()
	if err != nil {
		log.Println("error while announce tournament: can't begin transaction")
		return 0, err
	}

	lastID, err := model.TournamentAnnounce(tx, dbTournament)
	if err != nil {
		log.Println("provider: error while announce new tournament: ", err)
		tx.Rollback()
		return 0, err
	}

	err = tx.Commit()
	if err != nil {
		log.Println("error while commin tournament transaction: ", err)
		return 0, err
	}

	return lastID, err
}

// ChangePoints take/fund a player some points (with model func)
func ChangePoints(changePointsInput *model.ChangePointsJSONInput, path string) (*model.ChangePointsJSONOutput, error) {
	changePointsOutput := &model.ChangePointsJSONOutput{}

	badRequestErr := CheckChangePointsParams(changePointsInput)
	if badRequestErr != nil {
		log.Println("warning: can't change points: bad request params: ", badRequestErr)
		return nil, badRequestErr
	}

	tx, err := database.Begin()
	if err != nil {
		log.Println("error while change points: can't begin transaction")
		return nil, err
	}

	switch path {
	case "/take":
		changePointsOutput.Points, err = takePlayerPoints(tx, changePointsInput.PlayerName, changePointsInput.Points)
	case "/fund":
		changePointsOutput.NewPlayerCreated, changePointsOutput.Points, err = fundPlayerPoints(tx, changePointsInput.PlayerName, changePointsInput.Points)
	default:
		return nil, errors.New("provider: error while change player's points: wrong path")
	}

	if err != nil {
		log.Println("provider: error while change points: can't get player: ", err)
		tx.Rollback()
		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		log.Println("provider: error while change points: error while commin tournament transaction: ", err)
		return nil, err
	}

	return changePointsOutput, nil
}

func takePlayerPoints(executor model.QueryExecutor, playerName string, takePoints uint32) (uint32, error) {
	player, err := model.GetPlayerByName(executor, playerName)
	if err != nil {
		log.Println("provider: error while change points: can't get player: ", err)
		return 0, err
	}

	if player.Points < takePoints {
		log.Println("provider: error while take points: can't change points: player balance will be less then 0")
		return 0, model.NewBadRequestError("points", "player balance will be less then 0")
	}

	err = model.SetPointsByID(executor, player.ID, player.Points-takePoints)
	if err != nil {
		log.Println("provider: error while change points: ", err.Error())
		return 0, err
	}

	return player.Points - takePoints, nil
}

func fundPlayerPoints(executor model.QueryExecutor, playerName string, fundPoints uint32) (bool, uint32, error) {
	var isNew bool
	player, err := model.GetPlayerByName(executor, playerName)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			playerID, err := addNewPlayer(executor, playerName)
			if err != nil {
				return false, 0, err
			}
			isNew = true
			player = &model.Player{ID: playerID, Points: 0, Name: playerName}
		default:
			log.Println("provider: error while fund points: can't get player: ", err)
			return false, 0, err
		}
	}

	err = model.SetPointsByID(executor, player.ID, player.Points+fundPoints)
	if err != nil {
		log.Println("provider: error while fund points: ", err.Error())
		return false, 0, err
	}

	return isNew, player.Points + fundPoints, nil
}

// addNewPlayer call when client want to fund, but player not exist
func addNewPlayer(executor model.QueryExecutor, name string) (int64, error) {
	id, err := model.AddPlayer(executor, name)
	if err != nil {
		log.Println("error while add new player: " + err.Error())
		return 0, err
	}

	return id, nil
}

// JoinTournament do work to check players ability to join tournament and change DB
func JoinTournament(joinParams *model.JoinTournamentJSONInput) error {
	badRequestErr := CheckJoinTournamentParams(joinParams)
	if badRequestErr != nil {
		log.Println("warning: can't join to tournament: bad request params: ", badRequestErr)
		return badRequestErr
	}

	tx, err := database.Begin()
	if err != nil {
		log.Println("error while join tournament: can't begin transaction")
		return err
	}

	tournament, err := checkTournamentJoinAbility(tx, joinParams)
	if err != nil {
		log.Println("error while join tournament: can't check tournament ability")
		tx.Rollback()
		return err
	}

	player, backers, err := checkPlayerJoinAbility(tx, joinParams, tournament)
	if err != nil {
		log.Println("error while check player join ability: " + err.Error())
		tx.Rollback()
		return err
	}

	err = addPlayer(tx, tournament, player, tournament.Deposit)
	if err != nil {
		log.Println("error while add player: " + err.Error())
		tx.Rollback()
		return err
	}

	err = takePointsFromPlayers(tx, tournament, player, backers, tournament.Deposit)
	if err != nil {
		log.Println("error while take points from players: " + err.Error())
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		log.Println("error while commin tournament transaction: ", err)
		return err
	}

	return nil
}

func addPlayer(executor model.QueryExecutor, tournament *model.Tournament, player *model.Player, deposit uint32) error {
	err := model.UpdateTournamentPlayers(executor, append(tournament.Players, player.ID), tournament.ID)
	if err != nil {
		return err
	}

	return nil
}

func takePointsFromPlayers(executor model.QueryExecutor, tournament *model.Tournament, player *model.Player, backers []*model.Player, deposit uint32) error {
	participatoryShares := getParticipatoryShares(len(backers), deposit)

	if len(backers) != 0 {
		err := takePointsFromBackers(executor, backers, participatoryShares)
		if err != nil {
			log.Println("error while take points from backer: " + err.Error())
			return err
		}

		_, err = model.AddNewBackers(executor, player.Name, player.ID, tournament.ID, getBackersIDs(backers), participatoryShares)
		if err != nil {
			log.Println("error while add new backers: " + err.Error())
			return err
		}
	}

	err := model.SetPointsByID(executor, player.ID, player.Points-participatoryShares)
	if err != nil {
		log.Println("error while take points from player: " + err.Error())
		return err
	}

	return nil
}

func takePointsFromBackers(executor model.QueryExecutor, backers []*model.Player, depositPart uint32) error {
	if depositPart == 0 {
		return errors.New("error while take points from backers: deposit part is 0")
	}

	for _, v := range backers {
		err := model.SetPointsByID(executor, v.ID, v.Points-depositPart)
		if err != nil {
			return err
		}
	}

	return nil
}

func checkTournamentJoinAbility(executor model.QueryExecutor, joinParams *model.JoinTournamentJSONInput) (*model.Tournament, error) {
	if joinParams == nil {
		return nil, errors.New("error while check join tournament ability: join params is nil")
	}

	tournament, err := model.GetTournamentByID(executor, joinParams.TournamentID)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return nil, model.NewNotFoundError("tournament not found")
		default:
			return nil, err
		}
	}

	if uint32(len(tournament.Players)) >= tournament.MaxPlayers {
		return tournament, model.NewWrongStateError("tornament players count is max")
	}

	if tournament.EndTime.Before(time.Now()) {
		return tournament, model.NewWrongStateError("tornament end time already expire")
	}

	return tournament, nil
}

func checkPlayerJoinAbility(executor model.QueryExecutor, joinParams *model.JoinTournamentJSONInput, tournament *model.Tournament) (*model.Player, []*model.Player, error) {
	var player *model.Player
	var backers []*model.Player
	var err error

	if joinParams == nil {
		return nil, nil, errors.New("error while check player join ability: join params is nil")
	}

	player, err = model.GetPlayerByName(executor, joinParams.PlayerName)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return nil, nil, model.NewNotFoundError("player not found")
		default:
			return nil, nil, err
		}
	}

	if contains(tournament.Players, player.ID) {
		return nil, nil, model.NewWrongStateError("already joined")
	}

	if player.Points < tournament.Deposit && len(joinParams.BackersNames) == 0 {
		return nil, nil, model.NewWrongStateError("too few points")
	}

	if len(joinParams.BackersNames) != 0 {

		participatoryShares := getParticipatoryShares(len(joinParams.BackersNames), tournament.Deposit)
		if player.Points < participatoryShares {
			return nil, nil, model.NewWrongStateError("too few points (have no participatory shares)")
		}

		backers, err = checkBackers(executor, joinParams, participatoryShares)
		if err != nil {
			return nil, nil, err
		}
	}

	return player, backers, nil
}

func getParticipatoryShares(lenOfBackers int, deposit uint32) uint32 {
	if lenOfBackers == 0 {
		return deposit
	}

	return deposit / uint32((lenOfBackers + 1))
}

func checkBackers(executor model.QueryExecutor, joinParams *model.JoinTournamentJSONInput, depositPart uint32) ([]*model.Player, error) {
	backers := []*model.Player{}

	if joinParams == nil {
		return nil, errors.New("error while check backer join ability: join params is nil")
	}

	if depositPart == 0 {
		return nil, errors.New("error while chacke backer join ability: deposit part is 0")
	}

	if len(joinParams.BackersNames) == 0 {
		return nil, model.NewWrongStateError("have no backers")
	}

	for _, v := range joinParams.BackersNames {
		backer, err := checkPlayerToBeBacker(executor, v, depositPart)
		if err != nil {
			return nil, err
		}

		backers = append(backers, backer)
	}

	return backers, nil
}

func checkPlayerToBeBacker(executor model.QueryExecutor, backerName string, depositPart uint32) (*model.Player, error) {
	if backerName == "" {
		return nil, errors.New("error while check player to be backer: backer name is empty")
	}

	if depositPart == 0 {
		return nil, errors.New("error while check player to be backer: deposit part is 0")
	}

	backer, err := model.GetPlayerByName(executor, backerName)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return nil, model.NewNotFoundError("backer not found")
		default:
			return nil, err
		}
	}

	if backer.Points < depositPart {
		return nil, model.NewWrongStateError("backer have too few points")
	}

	return backer, nil
}

// ResultTournament calculate tournament winners if it not exist and return result
func ResultTournament(tournamentID int64) (*model.ResultTournamentJSONOutput, error) {
	if tournamentID == 0 {
		log.Println("error while get tournament result: tournament ID is 0")
		return nil, model.NewBadRequestError("tournament_id", "tournament ID is 0")
	}

	tx, err := database.Begin()
	if err != nil {
		log.Println("error while change points: can't begin transaction")
		return nil, err
	}

	tournament, err := model.GetTournamentByID(tx, tournamentID)
	if err != nil {
		log.Println("error while get tournament result: " + err.Error())
		tx.Rollback()

		if err == sql.ErrNoRows {
			return nil, model.NewNotFoundError("tournament not found")
		}

		return nil, err
	}

	if uint32(len(tournament.Players)) < jsonconfig.Tournament().MinPlayers {
		log.Println("error while get tournament result: too few players")
		tx.Rollback()
		return nil, model.NewWrongStateError("too few players")
	}

	// WARNING: not fully inpmlemented
	if len(tournament.Winners) == 0 {
		log.Println("winners not exist, update winners array")
		// for now tournament winner is first player in players array
		// because I don't know criterias of win
		winners, err := model.GetTournamentWinners(tx, tournament)
		if err != nil {
			log.Println("error while get tournament result: error while get winners: " + err.Error())
			tx.Rollback()
			return nil, err
		}

		err = model.UpdateTournamentWinners(tx, winners, tournamentID)
		if err != nil {
			log.Println("error while get tournament result: error while update winners: " + err.Error())
			tx.Rollback()
			return nil, err
		}

		tournament.Winners = winners

		err = fundPlayersAccordingToWinner(tx, tournament)
		if err != nil {
			log.Println("error while get tournament result: error while update winners: " + err.Error())
			tx.Rollback()
			return nil, err
		}
	}

	winnerNames, err := getWinnersNames(tx, tournament.Winners)
	if err != nil {
		log.Println("error while get tournament result: error while get winners names: " + err.Error())
		tx.Rollback()
		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		log.Println("error while commit tournament get winners transaction: ", err)
		return nil, err
	}

	return &model.ResultTournamentJSONOutput{Winners: winnerNames,
		Prize: tournament.Deposit * uint32(len(tournament.Players))}, nil
}

// PlayerBalance return current player balance
func PlayerBalance(playerName string) (*model.PlayerBalanceJSONOutput, error) {
	if playerName == "" {
		log.Println("error while get player balance: player ID is 0")
		return nil, model.NewBadRequestError("player_id", "player ID is 0")
	}

	tx, err := database.Begin()
	if err != nil {
		log.Println("error while change points: can't begin transaction")
		return nil, err
	}

	player, err := model.GetPlayerByName(tx, playerName)
	if err != nil {
		log.Println("error while get player balance: " + err.Error())
		tx.Rollback()

		if err == sql.ErrNoRows {
			return nil, model.NewNotFoundError("player not found")
		}

		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		log.Println("error while commit tournament get balance transaction: ", err)
		return nil, err
	}

	return &model.PlayerBalanceJSONOutput{PlayerID: player.ID,
		Balance: player.Points}, nil
}

// ResetDB return DB to initial state
func ResetDB() error {
	tx, err := database.Begin()
	if err != nil {
		log.Println("error while change points: can't begin transaction")
		return err
	}

	err = model.ResetDB(tx)
	if err != nil {
		log.Println("error while reset database: " + err.Error())
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		log.Println("error while commit tournament reset transaction: ", err)
		return err
	}

	return nil
}

func getWinnersNames(executor model.QueryExecutor, winners []int64) ([]string, error) {
	result := []string{}

	for _, v := range winners {
		player, err := model.GetPlayerByID(executor, v)
		if err != nil {
			return nil, err
		}

		result = append(result, player.Name)
	}

	return result, nil
}

func fundPlayersAccordingToWinner(executor model.QueryExecutor, tournament *model.Tournament) error {
	if tournament == nil {
		return errors.New("error while fund players according to winner: tournament is nil")
	}

	if len(tournament.Winners) < 1 {
		return errors.New("error while fund players according to winner: tournament have no winners")
	}

	if uint32(len(tournament.Players)) < jsonconfig.Tournament().MinPlayers {
		return errors.New("error while fund players according to winner: tournament have too few players")
	}

	prizeForEveryPlayer := (tournament.Deposit * uint32(len(tournament.Players)) / uint32(len(tournament.Winners)))

	for _, v := range tournament.Winners {
		backers, err := model.GetBackers(executor, v, tournament.ID)
		if err != nil && err != sql.ErrNoRows {
			return errors.New("error while get backers: " + err.Error())
		}

		fundedPlayers := []int64{}
		if backers != nil {
			fundedPlayers = append(fundedPlayers, backers.Backers...)
		}

		fundedPlayers = append(fundedPlayers, v)

		log.Println("player backers: ", backers)
		err = fundTournamentWinner(executor, fundedPlayers, prizeForEveryPlayer)
		if err != nil {
			return err
		}
	}

	return nil
}

func fundTournamentWinner(executor model.QueryExecutor, winnerWithBackers []int64, prize uint32) error {
	if len(winnerWithBackers) < 1 {
		return errors.New("error while fund tournament winner: len of winner < 1")
	}

	if prize == 0 {
		return errors.New("error while fund tournament winner: prize is 0")
	}

	prizeForEveryPlayer := prize / uint32(len(winnerWithBackers))

	for _, v := range winnerWithBackers {
		player, err := model.GetPlayerByID(executor, v)
		if err != nil {
			return errors.New("error while fund tournament winners: " + err.Error())
		}

		err = model.SetPointsByID(executor, player.ID, player.Points+prizeForEveryPlayer)
		if err != nil {
			return errors.New("error while fund tournament winners: " + err.Error())
		}
	}

	return nil
}
