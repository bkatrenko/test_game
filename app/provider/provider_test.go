package provider

import (
	"errors"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"bitbucket.org/bkatrenko/test_game/app/model"

	"bitbucket.org/bkatrenko/test_game/app/tools/jsonconfig"
)

// this consts have another value in model for better testing
const (
	tournamentDeposit    = 10
	tournamentMaxPlayers = 2
	tournamentDuration   = 65
	fundPlayerAmount     = 2000

	joinTournametAdditionalPoints = 2000
)

func TestProvider(t *testing.T) {

	var lastPlayerName string
	var lastTournamentID int64

	// Load the configuration file
	err := jsonconfig.Load("../../config"+string(os.PathSeparator)+"config.json", jsonconfig.AppConfig)
	if err != nil {
		log.Fatal("error while parse config file: ", err)
	}

	t.Run("TestDBConnection", func(t *testing.T) {
		err = ConnectToDB(jsonconfig.TestDatabase())
		if err != nil {
			t.Error("error while connect to DB: ", err)
		}
	})

	t.Run("TestGetDBConnection", func(t *testing.T) {
		db := GetDBConnection()
		if db == nil {
			t.Error("errpr while test get db connection: db is nil")
		}

		err := db.Ping()
		if err != nil {
			t.Error("error while test get db connection: can't ping DB")
		}
	})

	t.Run("TestAnnounceTournament", func(t *testing.T) {
		lastID, err := AnnounceTournament(&model.TournamentJSONInput{
			Deposit:     tournamentDeposit,
			Players:     tournamentMaxPlayers,
			DurationSec: tournamentDuration,
		})

		if err != nil {
			t.Error("error while test announce tournament: " + err.Error())
		}

		if lastID == 0 || lastID < 0 {
			t.Error("error while test announce tournament: last ID has wrong value")
		}

		lastTournamentID = lastID
	})

	t.Run("TestFundPoints", func(t *testing.T) {
		lastPlayerName = fmt.Sprint(time.Now().UnixNano())
		fundOutput, err := ChangePoints(&model.ChangePointsJSONInput{PlayerName: lastPlayerName,
			Points: fundPlayerAmount}, "/fund")
		if err != nil {
			t.Error("error while fund player's points: " + err.Error())
			return
		}

		if fundOutput == nil {
			t.Error("error while fund player's points: outpur is nil")
			return
		}

		if fundOutput.Points != fundPlayerAmount {
			t.Error("error while fund player's points: wrong points")
			return
		}
	})

	t.Run("TestTakePoints", func(t *testing.T) {
		fundOutput, err := ChangePoints(&model.ChangePointsJSONInput{PlayerName: lastPlayerName, Points: fundPlayerAmount}, "/take")
		if err != nil {
			t.Error("error while fund player's points: " + err.Error())
			return
		}

		if fundOutput == nil {
			t.Error("error while fund player's points: outpur is nil")
			return
		}

		if fundOutput.NewPlayerCreated {
			t.Error("error while fund player's points: player already exist")
			return
		}
		if fundOutput.Points != 0 {
			t.Error("error while fund player's points: wrong points")
			return
		}

	})

	t.Run("TestJoinTournament", func(t *testing.T) {
		output, err := ChangePoints(&model.ChangePointsJSONInput{PlayerName: lastPlayerName, Points: joinTournametAdditionalPoints}, "/fund")
		if err != nil {
			t.Error(err)
		}

		if output == nil {
			t.Error(output)
		}

		secondPlayerName := fmt.Sprint(time.Now().UnixNano())
		output, err = ChangePoints(&model.ChangePointsJSONInput{PlayerName: secondPlayerName, Points: joinTournametAdditionalPoints}, "/fund")
		if err != nil {
			t.Error(err)
		}

		if output == nil {
			t.Error(output)
		}

		err = JoinTournament(&model.JoinTournamentJSONInput{PlayerName: lastPlayerName, TournamentID: lastTournamentID, BackersNames: []string{}})
		if err != nil {
			t.Error(err)
		}

		err = JoinTournament(&model.JoinTournamentJSONInput{PlayerName: secondPlayerName, TournamentID: lastTournamentID, BackersNames: []string{}})
		if err != nil {
			t.Error(err)
		}
	})

	t.Run("TestResultTournament", func(t *testing.T) {
		output, err := ResultTournament(lastTournamentID)
		if err != nil {
			t.Error(err)
		}

		if output == nil {
			t.Error(output)
		}

		if len(output.Winners) == 0 {
			t.Error(errors.New("winners length is 0"))
		}

		if output.Winners[0] != lastPlayerName {
			t.Error(errors.New("winners length is 0"))
		}

		if output.Prize != tournamentDeposit*2 {
			t.Error(errors.New("prize is wrong"))
		}
	})

	t.Run("TestGetPlayerBalance", func(t *testing.T) {
		output, err := PlayerBalance(lastPlayerName)
		if err != nil {
			t.Error(err)
		}

		if output == nil {
			t.Error(output)
		}

		player, err := model.GetPlayerByName(database, lastPlayerName)
		if err != nil {
			t.Error(err)
		}

		if output.PlayerID != player.ID {
			t.Error(errors.New("wrong player ID"))
		}

		if output.Balance != player.Points {
			t.Error(errors.New("wrong player points"))
		}

	})

	t.Run("TestCloseDBConnection", func(t *testing.T) {
		err := CloseDBConnection()
		if err != nil {
			t.Error("error while test close db connection: " + err.Error())
		}
	})
}
