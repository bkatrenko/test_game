package model

import "database/sql"

// QueryExecutor is wrapper for sql TX, DB and other things that can execute query strings
type QueryExecutor interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
	QueryRow(query string, args ...interface{}) *sql.Row
}
