package model

import (
	"errors"
	"log"
	"time"

	"github.com/lib/pq"
)

type ResultTournamentJSONOutput struct {
	Winners []string `json:"winners"`
	Prize   uint32   `json:"prize"`
}

// TournamentJSONInput represents client's input for request
type TournamentJSONInput struct {
	Deposit     uint32 `json:"deposit"`      // deposit for game
	Players     uint32 `json:"players"`      // max players count (should be more then 1)
	DurationSec uint32 `json:"duration_sec"` // max tournament duration
}

// TournamentJSONOutput represents api output for tournament response
type TournamentJSONOutput struct {
	Message string `json:"message"` // message for client (can be empty)
	ID      int64  `json:"id"`      // ID of announced tournament
}

// Tournament represents row in tournamet table
type Tournament struct {
	ID         int64     `db:"id" json:"id,omitempty"` // Don't use Id, use NoteID() instead for consistency with MongoDB
	Deposit    uint32    `db:"deposit" json:"deposit"`
	MaxPlayers uint32    `db:"max_players" json:"max_players"`
	Players    []int64   `db:"players" json:"players"`
	Winners    []int64   `db:"winners" json:"winners"`
	StartTime  time.Time `db:"start_time" json:"start_time"`
	EndTime    time.Time `db:"end_time" json:"end_time"`
}

// TournamentAnnounce do work to insert new tournament to database
func TournamentAnnounce(executor QueryExecutor, tournament *Tournament) (int64, error) {
	var lastID int64

	err := executor.QueryRow("INSERT INTO tournaments (deposit, max_players, start_time, end_time) VALUES ($1,$2,$3,$4) RETURNING id;",
		tournament.Deposit, tournament.MaxPlayers, tournament.StartTime, tournament.EndTime).Scan(&lastID)
	if err != nil {
		log.Println("error while do transaction of announce new tournament: " + err.Error())
		return 0, err
	}

	return lastID, nil
}

// GetTournamentByID return tournament by his int64 int ID
func GetTournamentByID(executor QueryExecutor, tournamentID int64) (*Tournament, error) {
	tournament := &Tournament{}
	players := pq.Int64Array{}
	winners := pq.Int64Array{}

	row := executor.QueryRow(`SELECT id, deposit, max_players, players, winners, start_time, end_time FROM tournaments WHERE id=$1;`, tournamentID)
	err := row.Scan(&tournament.ID, &tournament.Deposit, &tournament.MaxPlayers, &players, &winners, &tournament.StartTime, &tournament.EndTime)

	tournament.Players = []int64(players)
	tournament.Winners = []int64(winners)
	return tournament, err
}

// UpdateTournamentPlayers update players IDs array
func UpdateTournamentPlayers(executor QueryExecutor, players []int64, tournamentID int64) error {
	_, err := executor.Exec("UPDATE tournaments SET players = $2 WHERE id = $1;", tournamentID, pq.Array(players))
	if err != nil {
		return err
	}

	return nil
}

// UpdateTournamentWinners update winners IDs array
func UpdateTournamentWinners(executor QueryExecutor, winners []int64, tournamentID int64) error {
	_, err := executor.Exec("UPDATE tournaments SET winners = $2 WHERE id = $1;", tournamentID, pq.Array(winners))
	if err != nil {
		return err
	}

	return nil
}

// GetTournamentWinners calculate winners, for now not fully implemented
func GetTournamentWinners(executor QueryExecutor, tournament *Tournament) ([]int64, error) {
	if len(tournament.Players) == 0 {
		return nil, errors.New("can't get tournament winners: players array is nil")
	}

	return []int64{tournament.Players[0]}, nil
}
