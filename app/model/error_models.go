package model

// JSONError is model for all errors request in app
type JSONError struct {
	Type    string `json:"type"`
	Message string `json:"message"`
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Bad request error
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// BadRequestError represent bad input params - negative deposit for example
type BadRequestError struct {
	Field   string
	Details string
}

// NewBadRequestError super mini constructor for bad request errors
func NewBadRequestError(field, details string) *BadRequestError {
	return &BadRequestError{
		Field:   field,
		Details: details,
	}
}

func (e *BadRequestError) Error() string {
	return e.Field + ": " + e.Details
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Not Found Error
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// JoinTournamentError represent bad input params for joiningto tournament
type NotFoundError struct {
	Details string
}

// NewJoinTournamentError super mini constructor for join tournament errors
func NewNotFoundError(details string) *NotFoundError {
	return &NotFoundError{
		Details: details,
	}
}

func (e *NotFoundError) Error() string {
	return e.Details
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Wrong state error
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// JoinTournamentError represent bad input params for joiningto tournament
type WrongStateError struct {
	Details string
}

// NewWrongStateError super mini constructor for wrong state errors
func NewWrongStateError(details string) *WrongStateError {
	return &WrongStateError{
		Details: details,
	}
}

func (e *WrongStateError) Error() string {
	return e.Details
}
