package model

import (
	"errors"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"bitbucket.org/bkatrenko/test_game/app/tools/dbutils"
	"bitbucket.org/bkatrenko/test_game/app/tools/jsonconfig"
)

// this consts have another value in provider for better testing
const (
	tournamentDeposit    = 100
	tournamentMaxPlayers = 20
	tournamentDuration   = 61
	setPointsCount       = 10
)

func TestModel(t *testing.T) {

	var lastTournamentID int64
	var lastPlayerID int64

	var firstBackerID int64
	var lastBackerID int64
	lastPlayerName := fmt.Sprint(time.Now().UnixNano())

	// Load the configuration file
	err := jsonconfig.Load("../../config"+string(os.PathSeparator)+"config.json", jsonconfig.AppConfig)
	if err != nil {
		log.Fatal("error while parse config file: ", err)
	}

	db, err := dbutils.CreateNewDBConnection(jsonconfig.TestDatabase())
	if err != nil {
		log.Fatal("error while connect to DB: ", err)
	}

	defer db.Close()

	t.Run("TestAnnounceTournament", func(t *testing.T) {
		lastID, err := TournamentAnnounce(db, &Tournament{Deposit: tournamentDeposit,
			MaxPlayers: tournamentMaxPlayers,
			StartTime:  time.Now(),
			EndTime:    time.Now().Add(tournamentDuration * time.Second)})
		if err != nil {
			t.Fatal(err)
		}

		if lastID == 0 {
			t.Fatal(errors.New("last ID is 0"))
		}

		lastTournamentID = lastID
	})

	t.Run("TestAddPlayer", func(t *testing.T) {
		lastID, err := AddPlayer(db, lastPlayerName)
		if err != nil {
			t.Error("error while add player: " + err.Error())
			return
		}

		if lastID == 0 {
			t.Error("error while add player: last id is 0")
			return
		}

		lastPlayerID = lastID
	})

	t.Run("TestGetPlayer", func(t *testing.T) {
		player, err := GetPlayerByName(db, lastPlayerName)
		if err != nil {
			t.Error("error while get player: " + err.Error())
			return
		}

		if player == nil {
			t.Error("error while get player: player struct is nil")
			return
		}
	})

	t.Run("TestSetPoints", func(t *testing.T) {
		playerBefore, err := GetPlayerByName(db, lastPlayerName)
		if err != nil {
			t.Error("error while set points: " + err.Error())
			return
		}
		log.Println(playerBefore.Points)

		err = SetPointsByID(db, lastPlayerID, setPointsCount)
		if err != nil {
			t.Error("error while set points: " + err.Error())
			return
		}

		playerAfter, err := GetPlayerByName(db, lastPlayerName)
		if err != nil {
			t.Error("error while set points: " + err.Error())
			return
		}

		log.Println(playerAfter.Points)
		if playerBefore.Points+setPointsCount != playerAfter.Points {
			t.Error("player points is not equal")
		}
	})

	t.Run("TestAddNewBackers", func(t *testing.T) {
		var err error
		firstBackerID, err = AddPlayer(db, fmt.Sprint(time.Now().UnixNano()))
		if err != nil {
			t.Error(err)
		}

		lastBackerID, err = AddPlayer(db, fmt.Sprint(time.Now().UnixNano()))
		if err != nil {
			t.Error(err)
		}

		backersID, err := AddNewBackers(db, lastPlayerName, lastPlayerID, lastTournamentID, []int64{firstBackerID, lastBackerID}, tournamentDeposit)
		if err != nil {
			t.Error(err)
		}

		if backersID == 0 {
			t.Error(errors.New("error while add backers: id is nil"))
		}
	})

	t.Run("TestGetTournamentByID", func(t *testing.T) {
		tournament, err := GetTournamentByID(db, lastTournamentID)
		if err != nil {
			t.Error(err)
		}

		if tournament == nil {
			t.Error(errors.New("error while get tournament by ID: tournament is nil"))
		}
	})

	t.Run("TestUpdateTournamentPlayers", func(t *testing.T) {
		err := UpdateTournamentPlayers(db, []int64{firstBackerID, lastBackerID}, lastTournamentID)
		if err != nil {
			t.Error(err)
		}
	})

	t.Run("TestGetPlayerByID", func(t *testing.T) {
		player, err := GetPlayerByID(db, lastPlayerID)
		if err != nil {
			t.Error(err)
		}

		if player.ID == 0 {
			t.Error(errors.New("player id is 0"))
		}
	})

	t.Run("TestUpdateTournamentWinners", func(t *testing.T) {
		err := UpdateTournamentWinners(db, []int64{lastPlayerID}, lastTournamentID)
		if err != nil {
			t.Error(err)
		}
	})

	t.Run("TestGetTournamentWinners", func(t *testing.T) {
		tournament, err := GetTournamentByID(db, lastTournamentID)
		if err != nil {
			t.Error(err)
		}

		winners, err := GetTournamentWinners(db, tournament)
		if err != nil {
			t.Error(err)
		}

		if len(winners) == 0 {
			t.Error("len of winners is 0")
		}
	})
}
