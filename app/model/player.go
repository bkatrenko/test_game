package model

import "log"

type PlayerBalanceJSONOutput struct {
	PlayerID int64  `json:"player_id"`
	Balance  uint32 `json:"balance"`
}

type JoinTournamentJSONInput struct {
	PlayerName   string   `json:"player_name"`   // player name, should be exist
	TournamentID int64    `json:"tournament_id"` // id of existing and not expierd tournament
	BackersNames []string `json:"backers"`       // backers list
}

// ChangePointsJSONInput represents json input object for change player's points
type ChangePointsJSONInput struct {
	PlayerName string `json:"player_name"` // player id, should be exist
	Points     uint32 `json:"points"`      // points what need to take
}

// ChangePointsJSONOutput represents json input object for change player's points
type ChangePointsJSONOutput struct {
	NewPlayerCreated bool   `json:"new_player_created"` // is new player created
	Points           uint32 `json:"points"`             // new points
}

// Player represents row in players table
type Player struct {
	ID     int64  `db:"id" json:"id,omitempty"` // Don't use Id, use NoteID() instead for consistency with MongoDB
	Name   string `db:"name" jsoon:"name"`
	Points uint32 `db:"points" json:"points"`
}

// GetPlayerByName return player info by name
func GetPlayerByName(executor QueryExecutor, name string) (*Player, error) {
	player := &Player{}

	row := executor.QueryRow(`SELECT id, points, name FROM players WHERE name=$1;`, name)
	err := row.Scan(&player.ID, &player.Points, &player.Name)

	return player, err
}

// GetPlayerByID return player info by id
func GetPlayerByID(executor QueryExecutor, playerID int64) (*Player, error) {
	player := &Player{}

	row := executor.QueryRow(`SELECT id, points, name FROM players WHERE id=$1;`, playerID)
	err := row.Scan(&player.ID, &player.Points, &player.Name)

	return player, err
}

// AddPlayer execute database query for new tournament creating
func AddPlayer(executor QueryExecutor, name string) (int64, error) {
	var lastID int64

	err := executor.QueryRow("INSERT INTO players (points, name) VALUES ($1, $2) RETURNING id;", 0, name).Scan(&lastID)
	if err != nil {
		log.Println("error while do transaction of add new player: " + err.Error())
		return 0, err
	}

	return lastID, nil
}

// SetPointsByID do setting player points
func SetPointsByID(executor QueryExecutor, playerID int64, points uint32) error {
	_, err := executor.Exec("UPDATE players SET points = $2 WHERE id = $1;", playerID, points)
	if err != nil {
		return err
	}

	return nil
}
