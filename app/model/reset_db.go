package model

// ResetDB return DB to initial state (truncate all tables)
func ResetDB(executor QueryExecutor) error {

	_, err := executor.Exec("TRUNCATE TABLE players CASCADE;")
	if err != nil {
		return err
	}

	_, err = executor.Exec("TRUNCATE TABLE tournaments CASCADE;")
	if err != nil {
		return err
	}

	_, err = executor.Exec("TRUNCATE TABLE backers CASCADE;")
	if err != nil {
		return err
	}

	return nil
}
