package model

import "log"
import "github.com/lib/pq"

// Backer represents row in players table
type Backer struct {
	ID           int64   `db:"id" json:"id,omitempty"` // Don't use Id, use NoteID() instead for consistency with MongoDB
	PlayerName   string  `db:"player_name" json:"player"`
	PlayerID     int64   `db:"player_id" json:"player_id"`
	TournamentID int64   `db:"tournament_id" json:"tournament_id"`
	Backers      []int64 `db:"backers" json:"backers"`
	Deposit      uint32  `db:"deposit" json:"deposit"`
}

// AddNewBackers add new backers to DB (without any take/fund)
func AddNewBackers(executor QueryExecutor, playerName string, playerID, tournamentID int64, backers []int64, deposit uint32) (int64, error) {
	var lastID int64

	err := executor.QueryRow("INSERT INTO backers (player_name, player_id, tournament_id, backers, deposit) VALUES ($1, $2, $3, $4, $5) RETURNING id;", playerName, playerID, tournamentID, pq.Array(backers), deposit).Scan(&lastID)
	if err != nil {
		log.Println("error while do transaction of add new player: " + err.Error())
		return 0, err
	}

	return lastID, nil
}

// GetBackers return players backers by tournament ID and player
func GetBackers(executor QueryExecutor, playerID, tournamentID int64) (*Backer, error) {
	backer := &Backer{}
	backers := pq.Int64Array{}

	row := executor.QueryRow(`SELECT id, player_name, player_id, tournament_id, backers, deposit FROM backers WHERE player_id=$1 AND tournament_id=$2;`, playerID, tournamentID)
	err := row.Scan(&backer.ID, &backer.PlayerName, &backer.PlayerID, &backer.TournamentID, &backers, &backer.Deposit)
	backer.Backers = backers

	if err != nil {
		return nil, err
	}

	return backer, nil
}
