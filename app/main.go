package main

import (
	"log"
	"net/http"
	"os"
	"runtime"

	"bitbucket.org/bkatrenko/test_game/app/provider"
	"bitbucket.org/bkatrenko/test_game/app/router"
	"bitbucket.org/bkatrenko/test_game/app/tools/jsonconfig"
)

func init() {
	// Verbose logging with file name and line number
	log.SetFlags(log.Lshortfile)

	// Use all CPU cores
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {

	// Load the configuration file
	err := jsonconfig.Load("../config"+string(os.PathSeparator)+"config.json", jsonconfig.AppConfig)
	if err != nil {
		log.Fatal("error while parse config file: ", err)
	}

	err = provider.ConnectToDB(jsonconfig.Database())
	if err != nil {
		log.Fatal("error while connect to DB: ", err)
	}

	err = http.ListenAndServe(":"+jsonconfig.Server().Port, router.GetInitializedRouter())
	if err != nil {
		log.Fatalf("Failed to fire up server with error: %v\n", err)
	}
}
