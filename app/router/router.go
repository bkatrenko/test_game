package router

import (
	"bitbucket.org/bkatrenko/test_game/app/controller"
	"github.com/gorilla/mux"
)

// GetInitializedRouter init router with controller funcs
func GetInitializedRouter() *mux.Router {
	//Init router
	router := mux.NewRouter()
	router.StrictSlash(true)

	router.HandleFunc("/announceTournament", controller.AnnounceTournament).Methods("POST")
	router.HandleFunc("/take", controller.ChangePoints).Methods("PATCH")
	router.HandleFunc("/fund", controller.ChangePoints).Methods("PATCH")
	router.HandleFunc("/joinTournament", controller.JoinTournament).Methods("POST")
	router.HandleFunc("/resultTournament/{id}", controller.ResultTournament).Methods("GET")
	router.HandleFunc("/balance/{name}", controller.PlayerBalance).Methods("GET")
	router.HandleFunc("/reset", controller.Reset).Methods("PATCH")

	return router
}
