package router

import (
	"testing"
)

func TestGetInitializedRouter(t *testing.T) {
	r := GetInitializedRouter()
	if r == nil {
		t.Fatal("error while TestGetInitializedRouter: router is nil")
	}
}
