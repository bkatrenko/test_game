package controller

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/gorilla/mux"

	"bitbucket.org/bkatrenko/test_game/app/model"
	"bitbucket.org/bkatrenko/test_game/app/provider"
	"bitbucket.org/bkatrenko/test_game/app/tools/jsonconfig"
)

const (
	testPlayerBalanceAfterWin = 1100
)

// TestController starts one test functions and few sub-tests because need loaded json config
// and DB connection
func TestController(t *testing.T) {

	var lastPlayerName string  // for test-created user
	var lastTournamentID int64 // for test-created tournament ID

	// Load the configuration file
	err := jsonconfig.Load("../../config"+string(os.PathSeparator)+"config.json", jsonconfig.AppConfig)
	if err != nil {
		log.Fatal("error while parse config file: ", err)
	}

	// Connecto provider to DB
	err = provider.ConnectToDB(jsonconfig.TestDatabase())
	if err != nil {
		log.Fatal("error while connect to DB: ", err)
	}

	defer provider.CloseDBConnection()

	t.Run("TestAnnounceTournament", func(t *testing.T) {
		err := testRequest(AnnounceTournament, http.StatusOK, "/announceTournament", "POST", []byte(`{"deposit":100, "players":10, "duration_sec":100}`), true,
			func(body []byte) error {
				response := &model.TournamentJSONOutput{}
				err := json.Unmarshal(body, response)
				if err != nil {
					return err
				}

				if response.ID == 0 {
					return errors.New("error while test body: wrong tournament ID")
				}

				if response.Message != "success" {
					return errors.New("error while test body: wrong message")
				}

				lastTournamentID = response.ID
				return nil
			})
		if err != nil {
			t.Error(err)
		}
	})

	t.Run("TestFundPoints", func(t *testing.T) {

		lastPlayerName = fmt.Sprint(time.Now().UnixNano())
		err := testRequest(ChangePoints, http.StatusOK, "/fund", "POST", []byte(`{"player_name":"`+lastPlayerName+`", "points":10}`), true,
			func(body []byte) error {
				response := &model.ChangePointsJSONOutput{}
				err := json.Unmarshal(body, response)
				if err != nil {
					return err
				}

				if !response.NewPlayerCreated {
					return errors.New("error while test body: should be new player created")
				}

				if response.Points != 10 {
					return errors.New("error while test body: points should be 10")
				}

				return nil
			})
		if err != nil {
			t.Error(err)
		}
	})

	t.Run("TestTakePoints", func(t *testing.T) {
		err := testRequest(ChangePoints, http.StatusOK, "/take", "POST", []byte(`{"player_name":"`+lastPlayerName+`", "points":10}`), true,
			func(body []byte) error {
				response := &model.ChangePointsJSONOutput{}
				err := json.Unmarshal(body, response)
				if err != nil {
					return err
				}

				if response.NewPlayerCreated {
					return errors.New("error while test body: new player created, but expect existing player")
				}

				if response.Points != 0 {
					return errors.New("error while test body: points should be 0")
				}

				return nil
			})
		if err != nil {
			t.Error(err)
		}
	})

	t.Run("TestJoinTournament", func(t *testing.T) {
		_, err := provider.ChangePoints(&model.ChangePointsJSONInput{PlayerName: lastPlayerName, Points: 1000}, "/fund")
		if err != nil {
			t.Error(err)
			return
		}

		playerNames := []string{"", "JohnDoe", lastPlayerName}
		expectedCodes := []int{400, 404, 200}

		for i := 0; i < len(expectedCodes); i++ {
			err = testRequest(JoinTournament, expectedCodes[i], "/joinTournament", "POST", []byte(`{"player_name":"`+playerNames[i]+`", "tournament_id":`+fmt.Sprint(lastTournamentID)+`, "backers":[]}`), false,
				func(body []byte) error {
					return nil
				})
			if err != nil {
				t.Error(err)
			}
		}

		tournamentIDs := []int64{-1, 12, lastTournamentID}
		expectedCodes = []int{400, 404, 406}

		for i := 0; i < len(expectedCodes); i++ {
			err = testRequest(JoinTournament, expectedCodes[i], "/joinTournament", "POST", []byte(`{"player_name":"`+lastPlayerName+`", "tournament_id":`+fmt.Sprint(tournamentIDs[i])+`, "backers":[]}`), false,
				func(body []byte) error {
					return nil
				})
			if err != nil {
				t.Error(err)
			}
		}
	})

	t.Run("TestJoinTournamentWithBackers", func(t *testing.T) {
		p1 := fmt.Sprint(time.Now().UnixNano())
		b1 := fmt.Sprint(time.Now().UnixNano())
		b2 := fmt.Sprint(time.Now().UnixNano())

		_, err := provider.ChangePoints(&model.ChangePointsJSONInput{PlayerName: p1, Points: 1000}, "/fund")
		if err != nil {
			t.Error(err)
			return
		}

		_, err = provider.ChangePoints(&model.ChangePointsJSONInput{PlayerName: b1, Points: 1000}, "/fund")
		if err != nil {
			t.Error(err)
			return
		}

		_, err = provider.ChangePoints(&model.ChangePointsJSONInput{PlayerName: b2, Points: 1000}, "/fund")
		if err != nil {
			t.Error(err)
			return
		}

		err = testRequest(JoinTournament, http.StatusOK, "/joinTournament", "POST", []byte(`{"player_name":"`+p1+`", "tournament_id":`+fmt.Sprint(lastTournamentID)+`, "backers":["`+b1+`", "`+b2+`"]}`), false,
			func(body []byte) error {
				return nil
			})
		if err != nil {
			t.Error(err)
		}
	})

	t.Run("TestGetResult", func(t *testing.T) {
		err := testRequestWithNewRouter(ResultTournament, http.StatusOK, "/resultTournament/{id}", "/resultTournament/"+fmt.Sprint(lastTournamentID), "GET", nil, true,
			func(body []byte) error {
				response := &model.ResultTournamentJSONOutput{}
				err := json.Unmarshal(body, response)
				if err != nil {
					return err
				}

				if response.Prize == 0 {
					return errors.New("prize is 0")
				}

				if len(response.Winners) == 0 {
					return errors.New("empty winners list")
				}

				return nil
			})

		if err != nil {
			t.Error(err)
		}
	})

	t.Run("TestGetBalance", func(t *testing.T) {
		err := testRequestWithNewRouter(PlayerBalance, http.StatusOK, "/balance/{name}", "/balance/"+lastPlayerName, "GET", nil, true,
			func(body []byte) error {
				response := &model.PlayerBalanceJSONOutput{}
				err := json.Unmarshal(body, response)
				if err != nil {
					return err
				}

				if response.PlayerID == 0 {
					return errors.New("player id is 0")
				}

				if response.Balance != testPlayerBalanceAfterWin {
					return errors.New("player balance is wrong")
				}

				return nil
			})

		if err != nil {
			t.Error(err)
		}
	})

	t.Run("TestGameLogic", func(t *testing.T) {
		_, err := provider.ChangePoints(&model.ChangePointsJSONInput{PlayerName: "P1", Points: 300}, "/fund")
		if err != nil {
			t.Error(err)
		}
		_, err = provider.ChangePoints(&model.ChangePointsJSONInput{PlayerName: "P2", Points: 300}, "/fund")
		if err != nil {
			t.Error(err)
		}

		_, err = provider.ChangePoints(&model.ChangePointsJSONInput{PlayerName: "P3", Points: 300}, "/fund")
		if err != nil {
			t.Error(err)
		}

		_, err = provider.ChangePoints(&model.ChangePointsJSONInput{PlayerName: "P4", Points: 500}, "/fund")
		if err != nil {
			t.Error(err)
		}

		_, err = provider.ChangePoints(&model.ChangePointsJSONInput{PlayerName: "P5", Points: 1000}, "/fund")
		if err != nil {
			t.Error(err)
		}

		tournamentID, err := provider.AnnounceTournament(&model.TournamentJSONInput{Deposit: 1000, Players: 10, DurationSec: 3600})
		if err != nil {
			t.Error(err)
		}

		err = provider.JoinTournament(&model.JoinTournamentJSONInput{PlayerName: "P1",
			TournamentID: tournamentID,
			BackersNames: []string{"P2", "P3", "P4"}})
		if err != nil {
			t.Error(err)
		}

		err = provider.JoinTournament(&model.JoinTournamentJSONInput{PlayerName: "P5",
			TournamentID: tournamentID,
			BackersNames: []string{}})
		if err != nil {
			t.Error(err)
		}

		res, err := provider.ResultTournament(tournamentID)
		if err != nil {
			t.Error(err)
		}

		if res.Winners[0] != "P1" {
			t.Error("wrong winner")
		}

		if res.Prize != 2000 {
			t.Error("wrong prize")
		}

		p1Balance, err := provider.PlayerBalance("P1")
		if err != nil {
			t.Error(err)
		}

		p2Balance, err := provider.PlayerBalance("P2")
		if err != nil {
			t.Error(err)
		}

		p3Balance, err := provider.PlayerBalance("P3")
		if err != nil {
			t.Error(err)
		}

		p4Balance, err := provider.PlayerBalance("P4")
		if err != nil {
			t.Error(err)
		}

		p5Balance, err := provider.PlayerBalance("P5")
		if err != nil {
			t.Error(err)
		}

		if p1Balance.Balance != 550 || p2Balance.Balance != 550 || p3Balance.Balance != 550 {
			t.Error("p1 or p2 or p3 player balance is wrong")
		}

		if p4Balance.Balance != 750 {
			t.Error("p4 player balance is wrong")
		}

		if p5Balance.Balance != 0 {
			t.Error("p5 player balance is wrong")
		}
	})

	t.Run("TestReset", func(t *testing.T) {
		err := testRequest(Reset, http.StatusOK, "/reset", "PATCH", nil, false,
			func(body []byte) error {

				return nil
			})

		if err != nil {
			t.Error(err)
		}
	})
}

func testRequest(handler http.HandlerFunc, expectedCode int, route, method string, body []byte, expectBody bool, bodyChecker func(body []byte) error) error {
	req, err := http.NewRequest(method, route, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	rr := httptest.NewRecorder()

	handler.ServeHTTP(rr, req)
	body, err = ioutil.ReadAll(rr.Body)

	log.Println(string(body))
	if err != nil {
		return err
	}

	if len(body) == 0 && expectBody {
		return errors.New("fail TestBestRates: body is empty")
	}

	err = bodyChecker(body)
	if err != nil {
		return err
	}

	if status := rr.Code; status != expectedCode {
		return fmt.Errorf("handler returned wrong status code: got %v want %v", status, expectedCode)
	}

	return nil
}

func testRequestWithNewRouter(handler http.HandlerFunc, expectedCode int, route, requestRoute, method string, body []byte, expectBody bool, bodyChecker func(body []byte) error) error {
	req, err := http.NewRequest(method, requestRoute, bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	rr := httptest.NewRecorder()

	router := mux.NewRouter()
	router.StrictSlash(true)

	router.HandleFunc(route, handler).Methods(method)
	router.ServeHTTP(rr, req)
	body, err = ioutil.ReadAll(rr.Body)

	log.Println(string(body))
	if err != nil {
		return err
	}

	if len(body) == 0 && expectBody {
		return errors.New("fail TestBestRates: body is empty")
	}

	err = bodyChecker(body)
	if err != nil {
		return err
	}

	if status := rr.Code; status != expectedCode {
		return fmt.Errorf("handler returned wrong status code: got %v want %v", status, expectedCode)
	}

	return nil
}
