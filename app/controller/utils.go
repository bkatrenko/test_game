package controller

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/bkatrenko/test_game/app/model"
)

// ReturnCodeJSONResponse return marshaled all type response
func ReturnCodeJSONResponse(w http.ResponseWriter, code int, resp interface{}) {
	bs, err := json.Marshal(resp)
	if err != nil {
		ReturnCodeJSONError(w, http.StatusInternalServerError, "internal error", "can't marshal response")
		return
	}

	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json")
	w.Write(bs)
}

// ReturnCodeJSONError return marshaled error
func ReturnCodeJSONError(w http.ResponseWriter, code int, errorType, message string) {
	bs, err := json.Marshal(model.JSONError{Type: errorType, Message: message})
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json")
	w.Write(bs)
}
