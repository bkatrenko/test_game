package controller

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"bitbucket.org/bkatrenko/test_game/app/model"
	"bitbucket.org/bkatrenko/test_game/app/provider"
)

// AnnounceTournament do announce new tournament specifying the entry deposit
func AnnounceTournament(w http.ResponseWriter, r *http.Request) {
	body, readErr := ioutil.ReadAll(r.Body)
	if readErr != nil {
		log.Println("error while announce new tournament: can't read request body: " + readErr.Error())
		ReturnCodeJSONError(w, http.StatusInternalServerError, "io_error", "can't read request body")
		return
	}

	if len(body) == 0 {
		log.Println("error while announce new tournament: empty json payoload")
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", "request body is empty, but expected new tournament")
		return
	}

	log.Println("announce new tournament: ", string(body))
	tournamentRequest := &model.TournamentJSONInput{}

	err := json.Unmarshal(body, tournamentRequest)
	if err != nil {
		log.Println("error while announce new tournament: can't unmarshal request")
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", "bad request model - can't parse body")
		return
	}

	ID, err := provider.AnnounceTournament(tournamentRequest)
	switch err.(type) {
	case *model.BadRequestError:
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", err.Error())
		return
	case nil:
		ReturnCodeJSONResponse(w, http.StatusOK, model.TournamentJSONOutput{Message: "success", ID: ID})
		return
	default:
		log.Println("error while announce new tournament: " + err.Error())
		ReturnCodeJSONError(w, http.StatusInternalServerError, "internal_error", err.Error())
		return
	}
}

// JoinTournament join players to tournament
func JoinTournament(w http.ResponseWriter, r *http.Request) {
	body, readErr := ioutil.ReadAll(r.Body)
	if readErr != nil {
		log.Println("error while join player to tournament: can't read request body: " + readErr.Error())
		ReturnCodeJSONError(w, http.StatusInternalServerError, "io_error", "can't read request body")
		return
	}

	if len(body) == 0 {
		log.Println("error while join player to tournament: empty json payoload")
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", "request body is empty, but expected new tournament")
		return
	}

	log.Println("join player to tournament: ", string(body))
	joinTournamentRequest := &model.JoinTournamentJSONInput{}

	err := json.Unmarshal(body, joinTournamentRequest)
	if err != nil {
		log.Println("error while join players to tournament: can't unmarshal request")
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", "bad request model - can't parse body")
		return
	}

	err = provider.JoinTournament(joinTournamentRequest)
	switch err.(type) {
	case *model.BadRequestError:
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", err.Error())
		return

	case *model.NotFoundError:
		ReturnCodeJSONError(w, http.StatusNotFound, "not_found", err.Error())
		return
	case *model.WrongStateError:
		ReturnCodeJSONError(w, http.StatusNotAcceptable, "wrong_state", err.Error())
		return
	case nil:
		return
	default:
		log.Println("error while add player to tournament: " + err.Error())
		ReturnCodeJSONError(w, http.StatusInternalServerError, "internal_error", err.Error())
		return
	}
}

// ChangePoints takes/fund some points (with provider func)
func ChangePoints(w http.ResponseWriter, r *http.Request) {
	changePointsInput := getChangePointsRequestInput(r, w)
	if changePointsInput == nil {
		return
	}

	result, err := provider.ChangePoints(changePointsInput, r.URL.Path)
	switch err.(type) {
	case *model.BadRequestError:
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", err.Error())
		return
	case nil:
		ReturnCodeJSONResponse(w, http.StatusOK, result)
		return
	default:
		if err == sql.ErrNoRows {
			ReturnCodeJSONError(w, http.StatusNotFound, "not_found", "player not found")
			return
		}

		log.Println("error take points: " + err.Error())
		ReturnCodeJSONError(w, http.StatusInternalServerError, "internal_error", err.Error())
		return
	}
}

// ResultTournament get result of existing tournament
func ResultTournament(w http.ResponseWriter, r *http.Request) {
	tournamentIDRaw := mux.Vars(r)["id"]

	if tournamentIDRaw == "" {
		log.Println("eror while get tornament result: tournament id is empty")
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", "tournament ID is empty")
		return
	}

	tournamentID, err := strconv.ParseInt(tournamentIDRaw, 10, 64)
	if err != nil {
		log.Println("eror while get tornament result: can't parse tournament ID: " + err.Error())
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", "can't parse tournament ID")
		return
	}

	response, err := provider.ResultTournament(tournamentID)
	switch err.(type) {
	case *model.BadRequestError:
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", err.Error())
		return
	case *model.NotFoundError:
		ReturnCodeJSONError(w, http.StatusNotFound, "not_found", err.Error())
		return
	case *model.WrongStateError:
		ReturnCodeJSONError(w, http.StatusNotAcceptable, "wrong_state", err.Error())
		return
	case nil:
		ReturnCodeJSONResponse(w, http.StatusOK, response)
		return
	default:
		log.Println("error while add player to tournament: " + err.Error())
		ReturnCodeJSONError(w, http.StatusInternalServerError, "internal_error", err.Error())
		return
	}
}

// PlayerBalance retun json output with current player balance
func PlayerBalance(w http.ResponseWriter, r *http.Request) {
	playerName := mux.Vars(r)["name"]

	if playerName == "" {
		log.Println("eror while get player balance:player name is empty")
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", "player name is empty")
		return
	}

	response, err := provider.PlayerBalance(playerName)
	switch err.(type) {
	case *model.BadRequestError:
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", err.Error())
		return
	case *model.NotFoundError:
		ReturnCodeJSONError(w, http.StatusNotFound, "not_found", err.Error())
		return
	case nil:
		ReturnCodeJSONResponse(w, http.StatusOK, response)
		return
	default:
		log.Println("error while get player balance: " + err.Error())
		ReturnCodeJSONError(w, http.StatusInternalServerError, "internal_error", err.Error())
		return
	}
}

// Reset return DB to initial state
func Reset(w http.ResponseWriter, r *http.Request) {
	err := provider.ResetDB()
	log.Println("reset DB")
	switch err.(type) {
	case nil:
		return
	default:
		log.Println("error while reset DB: " + err.Error())
		ReturnCodeJSONError(w, http.StatusInternalServerError, "internal_error", err.Error())
		return
	}
}

func getChangePointsRequestInput(r *http.Request, w http.ResponseWriter) *model.ChangePointsJSONInput {
	body, readErr := ioutil.ReadAll(r.Body)
	if readErr != nil {
		log.Println("error while " + r.URL.Path + " points: can't read request body: " + readErr.Error())
		ReturnCodeJSONError(w, http.StatusInternalServerError, "io_error", "can't read request body")
		return nil
	}

	if len(body) == 0 {
		log.Println("error while " + r.URL.Path + " players points: empty json payoload")
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", "request body is empty, but expected takeing points info")
		return nil
	}

	log.Println(r.URL.Path+" points: ", string(body))
	takePointsRequest := &model.ChangePointsJSONInput{}

	err := json.Unmarshal(body, takePointsRequest)
	if err != nil {
		log.Println("error while change points: can't unmarshal request")
		ReturnCodeJSONError(w, http.StatusBadRequest, "bad_request", "bad request model - can't parse body")
		return nil
	}

	return takePointsRequest
}
