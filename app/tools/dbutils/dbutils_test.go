package dbutils

import (
	"log"
	"os"
	"testing"

	"bitbucket.org/bkatrenko/test_game/app/tools/jsonconfig"
)

func TestCreateNewDBConnection(t *testing.T) {

	err := jsonconfig.Load("../../../config"+string(os.PathSeparator)+"config.json", jsonconfig.AppConfig)
	if err != nil {
		log.Fatal("error while parse config file: ", err)
	}

	db, err := CreateNewDBConnection(jsonconfig.TestDatabase())
	if err != nil {
		t.Fatal("error while TestCreateNewDBConnection: " + err.Error())
	}

	if db == nil {
		t.Fatal("error while TestCreateNewDBConnection: db is nil")
	}
}
