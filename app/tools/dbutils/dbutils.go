package dbutils

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	"bitbucket.org/bkatrenko/test_game/app/tools/jsonconfig"
	_ "github.com/lib/pq"
)

// CreateNewDBConnection open new DB connection and return pointer to wrapper
func CreateNewDBConnection(c *jsonconfig.DBConfig) (*sql.DB, error) {
	db, err := sql.Open("postgres", fmt.Sprintf(`
		host=%s 
		port=%s 
		user=%s 
		dbname=%s 
		password=%s 
		sslmode=%s`,
		c.Host, c.Port, c.UserName, c.DBName, c.Password, c.SSLMode))
	if err != nil {
		return nil, errors.New("error while connect to database: " + err.Error())
	}

	log.Println("successfully connected to database")
	return db, nil
}
