package jsonconfig

import (
	"log"
	"os"
	"testing"
)

func TestJSONConfig(t *testing.T) {
	// Load the configuration file
	err := Load("../../../config"+string(os.PathSeparator)+"config.json", AppConfig)
	if err != nil {
		log.Fatal("error while parse config file: ", err)
	}

	tournament := Tournament()
	if tournament == nil {
		t.Error("error while test json config: tournament config is nil")
	}

	database := Database()
	if database == nil {
		t.Error("error while test json config: database config is nil")
	}

	testDatabase := TestDatabase()
	if testDatabase == nil {
		t.Error("error while test json config: test database config is nil")
	}

	server := Server()
	if server == nil {
		t.Error("error while test json config: server config is nil")
	}
}
