package jsonconfig

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Application Settings

// DBConfig contains the database configurations
type DBConfig struct {
	DBType   string
	UserName string
	DBName   string
	Password string
	SSLMode  string
	Host     string
	Port     string
}

// TournamentSettings contains settings for tournaments in app
type TournamentSettings struct {
	MinDurationSec uint32 `json:"MinDurationSec"`
	MinDeposit     uint32 `json:"MinDeposit"`
	MaxPlayers     uint32 `json:"MaxPlayers"`
	MinPlayers     uint32 `json:"MinPlayers"`
}

// ServerSettings contains server preferences (always possible to add more settings)
type ServerSettings struct {
	Port string
}

// ApplicationConfiguration contains the application settings
type ApplicationConfiguration struct {
	Database     DBConfig           `json:"Database"`
	TestDatabase DBConfig           `json:"TestDatabase"`
	Server       ServerSettings     `json:"ServerSettings"`
	Tournament   TournamentSettings `json:"TournamentSettings"`
}

// Parser must implement ParseJSON
type Parser interface {
	ParseJSON([]byte) error
}

// AppConfig the settings variable
var AppConfig = &ApplicationConfiguration{}

// ParseJSON unmarshals bytes to structs
func (c *ApplicationConfiguration) ParseJSON(b []byte) error {
	return json.Unmarshal(b, &c)
}

// Database return current database connection info
func Database() *DBConfig {
	return &AppConfig.Database
}

// TestDatabase return database connection info for testing
// For now it's similar to production, but can be changed for another with np problem
func TestDatabase() *DBConfig {
	return &AppConfig.TestDatabase
}

// Server return current server preferences
func Server() *ServerSettings {
	return &AppConfig.Server
}

// Tournament return current tournaments preferences
func Tournament() *TournamentSettings {
	return &AppConfig.Tournament
}

// Load the JSON config file
func Load(configFile string, p Parser) error {
	var err error
	var absPath string
	var input = io.ReadCloser(os.Stdin)
	if absPath, err = filepath.Abs(configFile); err != nil {
		return err
	}

	if input, err = os.Open(absPath); err != nil {
		return err
	}

	// Read the config file
	jsonBytes, err := ioutil.ReadAll(input)
	input.Close()
	if err != nil {
		return err
	}

	// Parse the config
	if err := p.ParseJSON(jsonBytes); err != nil {
		return err
	}

	return nil
}
